import UIKit
import Foundation
import Alamofire
import SwiftKeychainWrapper
import Amplify

class PasswordResetInterstitialController: UIViewController {
    
    @IBOutlet var username: UITextField!
    
    @IBAction func didTapContinue(_ sender: Any) {
        let dispatchGroup = DispatchGroup()
        _ = Amplify.Auth.resetPassword(for: username.text ?? "") {(result) in
            do {
                let resetResult = try result.get()
                switch resetResult.nextStep {
                case .confirmResetPasswordWithCode(_, _):
                    dispatchGroup.notify(queue: .main) {
                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let passwordResetController = storyboard.instantiateViewController(withIdentifier: "PasswordResetController") as! PasswordResetController
                        passwordResetController.username = self.username.text!
                        self.present(passwordResetController, animated: true, completion: nil)
                    }
                case .done:
                    print("Reset completed")
                }
            } catch {
                switch error {
                case AuthError.notAuthorized(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                case AuthError.invalidState(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                case AuthError.configuration(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                case AuthError.service(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                case AuthError.sessionExpired(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                case AuthError.signedOut(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: errorDescription)
                    }
                default:
                    dispatchGroup.notify(queue: .main) {
                        self.userNotFoundMessage(message: "We ran into a problem")
                    }
                }
            }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func userNotFoundMessage(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
