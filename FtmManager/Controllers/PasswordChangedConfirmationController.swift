import UIKit
import Foundation
import SwiftKeychainWrapper
import Amplify
import AWSPluginsCore


class PasswordChangedConfirmationController: UIViewController {
    var authToken: String = ""

    
    @IBAction func didTapContinue(_ sender: Any) {
        _ = Amplify.Auth.fetchAuthSession { (result) in
            do {
                let session = try result.get()
                // Get cognito user pool token
                if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                    let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                    self.authToken = tokens.idToken
                    KeychainWrapper.standard.set(self.authToken, forKey: "authToken")
                    self.determineNextScreen()
                }
            } catch {
                self.determineNextScreen()
                print("Fetch auth session failed with error - \(error)")
            }
        }
    }
    
    
    func determineNextScreen() {
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.notify(queue: .main) {
            if self.authToken != "" {
                let containerViewController = storyboard.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
                UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                    UIApplication.shared.keyWindow?.rootViewController = containerViewController
                })
            } else {
                let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
                UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                    UIApplication.shared.keyWindow?.rootViewController = loginViewController
                })
            }
        }
    }
}
