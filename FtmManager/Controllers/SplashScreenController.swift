//
//  SplashScreenController.swift
//  FtmManager
//
//  Created by Julian Gallo on 6/22/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import Amplify
import AWSPluginsCore
import Alamofire

class SplashScreenController: UIViewController {
    
    var authToken: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        let logoImage = UIImageView()
        logoImage.image = UIImage(named: "logo")
        logoImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logoImage)
        
        [
            logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImage.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            logoImage.heightAnchor.constraint(equalToConstant: view.frame.width),
            logoImage.widthAnchor.constraint(equalToConstant: view.frame.width),
        ].forEach{ $0.isActive = true }

    }
    
    override func viewDidAppear(_ animated: Bool) {                
        _ = Amplify.Auth.fetchAuthSession { (result) in
            do {
                let session = try result.get()
                // Get cognito user pool token
                if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                    let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                    self.authToken = tokens.idToken
                    KeychainWrapper.standard.set(self.authToken, forKey: "authToken")
                    self.determineNextScreen()
                }
            } catch {
                self.determineNextScreen()
                print("Fetch auth session failed with error - \(error)")
            }
        }
    }
    
    func determineNextScreen() {
        let storyBoard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

        let dispatchGroup = DispatchGroup()
        dispatchGroup.notify(queue: .main) {
            if self.authToken != "" {
                let containerViewController = storyBoard.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
                UIApplication.shared.keyWindow?.rootViewController = containerViewController
            } else {
                let loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
                UIApplication.shared.keyWindow?.rootViewController = loginViewController
            }
        }
    }
}
