//
//  ProfileViewController.swift
//  FtmManager
//
//  Created by Julian Gallo on 5/12/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import OAuthSwift
import SwiftKeychainWrapper

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var truckName: UITextField!
    @IBOutlet var truckDescription: UITextView!
    @IBOutlet var truckWebsite: UITextField!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var twitterLoginButton: UIButton!
    @IBOutlet var twitterLogoutButton: UIButton!
    @IBOutlet var twitterHandleField: UITextField!
    @IBOutlet var profileContainer: UIView!
    @IBOutlet var truckNameDisplay: UITextField!
    
    let imagePicker = UIImagePickerController()
    var cameraAlert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    
    var twitterHandle = ""
    var twitterClient: OAuth1Swift? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleTwitterDisplay()
        
        self.handleProfileDisplay()
                
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }
        cameraAlert.addAction(cameraAction)
        cameraAlert.addAction(galleryAction)
        cameraAlert.addAction(cancelAction)
        cameraAlert.popoverPresentationController?.sourceView = self.view
        
        profileImage.layer.cornerRadius = self.profileImage.frame.height/2.25
        
        let orange = UIColor(red: 224/255, green: 109/255, blue: 23/255, alpha: 1.0)
        let pink = UIColor(red: 232/255, green: 23/255, blue: 120/255, alpha: 1.0)
        
        let gradient = CAGradientLayer()
        let bounds = CGRect(x: 0, y: 0, width: profileContainer.bounds.width, height: profileContainer.bounds.height)
        gradient.frame = bounds
        gradient.colors = [orange.cgColor, pink.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0, y: 1.0)
        
        profileContainer.layer.insertSublayer(gradient, at: 0)
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func handleProfileDisplay() {
        cognitoSession.request(
            backend_server_url + "api/profile/"
        ){ URLRequest in URLRequest.timeoutInterval = 10}.validate(statusCode: 200..<300).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    self.truckName.text = json["truck_name"] as? String ?? ""
                    self.truckNameDisplay.text = json["truck_name"] as? String ?? ""
                    self.truckDescription.text = json["truck_description"] as? String ?? ""
                    self.truckWebsite.text = json["truck_website"] as? String ?? ""
                    self.twitterHandle = json["twitter_account"] as? String ?? ""
                    
                    let truck_image_url = json["truck_image"] as? String ?? ""
                    if truck_image_url != "" {
                        AF.request(truck_image_url).validate(statusCode: 200..<300)
                            .responseData(completionHandler: { (responseData) in
                                self.profileImage.image = UIImage(data: responseData.data!)
                            })
                    }
                    self.handleTwitterDisplay()
                }
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    @IBAction func unwindToProfile(_ unwindSegue: UIStoryboardSegue) {
    }
    
    @IBAction func editProfileImage(_ sender: UIButton) {
        imagePicker.delegate = self
        present(cameraAlert, animated: true, completion: nil)
    }
    
    func openCamera(){
        cameraAlert.dismiss(animated: true, completion: nil)
        let cameraAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
        if cameraAvailable {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = true
                present(imagePicker, animated: true, completion: nil)
                
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = true
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                
            case .denied, .restricted:
                let alert = UIAlertController(title: "Wartning", message: "This app doesn't have permission to access your camera. Please grant access in your device's settings to proceed.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Wartning", message: "No camera found", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        cameraAlert.dismiss(animated: true, completion: nil)
        imagePicker.sourceType = .savedPhotosAlbum
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        let image :UIImage
        if info[.editedImage] != nil {
            image = info[.editedImage] as! UIImage
        } else {
            image = info[.originalImage] as! UIImage
        }
        profileImage.image = image.resizeImage(targetSize: CGSize(width:350, height:900))
    }
    
    @IBAction func saveTruckProfile(_ sender: UIButton) {
        var upload_image = false
        var profile_image: Data? = nil
        if self.profileImage.image?.pngData() != nil {
            upload_image = true
            profile_image = self.profileImage.image?.pngData()
        }
        let truck_name = self.truckName.text!.data(using: String.Encoding.utf8)!
        let truck_description = self.truckDescription.text!.data(using: String.Encoding.utf8)!
        let truck_website = self.truckWebsite.text!.data(using: String.Encoding.utf8)!
        
        cognitoSession.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(truck_name, withName: "truck_name", mimeType: "text")
                multipartFormData.append(truck_description, withName: "truck_description", mimeType: "text")
                multipartFormData.append(truck_website, withName: "truck_website", mimeType: "text")
                if upload_image {
                    multipartFormData.append(profile_image!, withName: "truck_image", fileName: "truck_image.png", mimeType: "image/png")
                }
        },
            to: "\(backend_server_url)api/profile/",
            method: .put
        ).validate(statusCode: 200..<300).response { response in
            switch response.result {
            case .success( _):
                self.performSegue(withIdentifier: "backToMap", sender: self)
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    func handleTwitterDisplay() {
        if twitterHandle == "" {
            twitterHandleField.isHidden = true
            twitterLogoutButton.isHidden = true
            twitterLoginButton.isHidden = false
        } else {
            twitterLoginButton.isHidden = true
            twitterLogoutButton.isHidden = false
            twitterHandleField.isHidden = false
            twitterHandleField.text = "@" + twitterHandle
            twitterHandleField.isUserInteractionEnabled = false;
        }
    }
    
    @IBAction func twitterLogoutPressed(_ sender: Any) {
        cognitoSession.request(
            backend_server_url + "api/social-platform-keys/twitter/",
            method: .delete,
            parameters: [:]
        ).validate(statusCode: 200..<300).response { response in
            switch response.result {
            case .success(_):
                self.twitterHandle = ""
                self.handleTwitterDisplay()
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    @IBAction func twitterLoginPressed(_ sender: Any) {
        var twitterApiKey = ""
        var twitterApiSecret = ""
                
        cognitoSession.request(
            backend_server_url + "api/social-media-auth-keys/",
            method: .get
        ).validate(statusCode: 200..<300).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    twitterApiKey = json["twitter_api_key"] as? String ?? ""
                    twitterApiSecret = json["twitter_api_secret"] as? String ?? ""
                }
                self.loginWithTwitter(twitterApiKey: twitterApiKey, twitterApiSecret: twitterApiSecret)
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    func loginWithTwitter(twitterApiKey: String, twitterApiSecret: String) {
        self.twitterClient = OAuth1Swift(
            consumerKey: twitterApiKey,
            consumerSecret: twitterApiSecret,
            requestTokenUrl: "https://api.twitter.com/oauth/request_token",
            authorizeUrl:    "https://api.twitter.com/oauth/authorize",
            accessTokenUrl:  "https://api.twitter.com/oauth/access_token"
        )
        self.twitterClient!.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: self.twitterClient!)
        self.twitterClient!.authorize(
        withCallbackURL: "oauth-swift://oauth-callback/twitter") { result in
            switch result {
            case .success(let(credential, _, parameters)):
                self.twitterHandle = parameters["screen_name"] as! String
                cognitoSession.request(
                    backend_server_url + "api/social-platform-keys/twitter/",
                    method: .post,
                    parameters: [
                        "token": credential.oauthToken,
                        "secret": credential.oauthTokenSecret,
                        "handle": self.twitterHandle
                    ]
                ).validate(statusCode: 200..<300).response{ response in
                    switch response.result {
                    case .success(_):
                        self.handleTwitterDisplay()
                    case .failure(_):
                        self.networkErrorMessage()
                    }
                }
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    func networkErrorMessage() {
        let alert = UIAlertController(title: "Error", message: "Network Error", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func passwordUpdated() {
        let alert = UIAlertController(title: "Success", message: "Password Updated", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
