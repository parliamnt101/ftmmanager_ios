import UIKit
import SwiftKeychainWrapper
import Alamofire
import Amplify

class SideMenuViewController: UIViewController, UINavigationControllerDelegate {
    weak var delegate: SideMenuDelegate?
    
    @IBOutlet var truckName: UITextField!
    @IBOutlet var truckDescription: UITextView!
    @IBOutlet var truckWebsite: UITextField!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var twitterHandle: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.handleProfileDisplay()
        
        profileImage.layer.cornerRadius = self.profileImage.frame.height/2.25
        
        let orange = UIColor(red: 224/255, green: 109/255, blue: 23/255, alpha: 1.0)
        let pink = UIColor(red: 232/255, green: 23/255, blue: 120/255, alpha: 1.0)
        
        let gradient = CAGradientLayer()
        var bounds = editButton.bounds
        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds
        gradient.colors = [orange.cgColor, pink.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: -1.0)
        gradient.endPoint = CGPoint(x: 0, y: 1.5)
        
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradient.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradient.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        
        editButton.setBackgroundImage(gradientImage, for: .normal)
        editButton.layer.cornerRadius = 12
        editButton.layer.masksToBounds = true
        
        truckDescription.layer.borderWidth = 0
    }
    
    func handleProfileDisplay() {
        cognitoSession.request(
            backend_server_url + "api/profile/"
        ){ URLRequest in URLRequest.timeoutInterval = 10}.validate(statusCode: 200..<300).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    self.truckName.text = json["truck_name"] as? String ?? ""
                    self.truckDescription.text = json["truck_description"] as? String ?? ""
                    self.truckWebsite.text = json["truck_website"] as? String ?? ""
                    self.twitterHandle.text = json["twitter_account"] as? String ?? ""
                    let truck_image_url = json["truck_image"] as? String ?? ""
                    if truck_image_url != "" {
                        AF.request(truck_image_url).validate()
                            .responseData(completionHandler: { (responseData) in
                                self.profileImage.image = UIImage(data: responseData.data!)
                            })
                    }
                }
            case .failure(_):
                return
            }
        }
    }
    
    @IBAction func logoutWasPressed(_ sender: UIButton) {
        KeychainWrapper.standard.remove(key: "authToken")
        _ = Amplify.Auth.signOut() { (result) in
            switch result {
            case .success:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let dispatchGroup = DispatchGroup()
                dispatchGroup.notify(queue: .main) {
                    let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
                    UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        UIApplication.shared.keyWindow?.rootViewController = loginViewController
                    })
                }
            case .failure(let error):
                print("Sign out failed with error \(error)")
            }
        }

    }
}
