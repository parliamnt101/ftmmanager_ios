import UIKit
import Foundation
import Alamofire
import SwiftKeychainWrapper
import Amplify
import AmplifyPlugins

class PasswordChangeController: UIViewController {
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var currentPassword: UITextField!
    
    @IBAction func UpdatePasswordClicked(_ sender: UIButton) {
        view.endEditing(true)
        if (confirmPassword.text == newPassword.text) {
            updatePassword()
        } else {
            displayAlert(message: "Passwords do not match")
        }
    }
    
    func updatePassword() {
        let dispatchGroup = DispatchGroup()
        _ = Amplify.Auth.update(oldPassword: (currentPassword.text ?? ""), to: (newPassword.text ?? "")) { result in
            switch result {
            case .success:
                let dispatchGroup = DispatchGroup()
                dispatchGroup.notify(queue: .main) {
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "passwordUpdated"), object: nil)
                    })
                }
            case .failure(let error):
                switch error {
                case AuthError.notAuthorized(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: errorDescription)
                    }
                case AuthError.invalidState(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: errorDescription)
                    }
                case AuthError.configuration(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: errorDescription)
                    }
                case AuthError.service(let errorDescription, _, let error):
                    switch error {
                    case AWSCognitoAuthError.invalidParameter?:
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: "Passwords must be at least 8 characters and have at least one uppercase, lowercase, special character, and number")
                        }
                    case AWSCognitoAuthError.codeExpired?:
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: "Your security code has expired")
                        }
                    case AWSCognitoAuthError.codeMismatch?:
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: "Incorrect recovery code")
                        }
                    default:
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    }
                case AuthError.sessionExpired(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: errorDescription)
                    }
                case AuthError.signedOut(let errorDescription, _, _):
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: errorDescription)
                    }
                default:
                    dispatchGroup.notify(queue: .main) {
                        self.displayAlert(message: "We ran into a problem")
                    }
                }
            }
        }
    }
    
    func displayAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
