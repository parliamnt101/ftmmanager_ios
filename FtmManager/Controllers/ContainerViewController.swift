//
//  ContainerViewController.swift
//  FtmManager
//
//  Created by Julian Gallo on 4/26/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import QuartzCore

class ContainerViewController: UIViewController {
    enum SlideOutState {
        case menuCollapsed
        case menuPanelExpanded
    }
   
    var mapNavigationController: UINavigationController!
    var mapViewController: MapViewController!
    
    var currentState: SlideOutState = .menuCollapsed
    var menuViewController: SideMenuViewController?
    
    let menuWidth: CGFloat = 0.75
    
    var overlay = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.overlay = UIView(frame: CGRect(x:CGFloat(0), y: CGFloat(0), width: self.view.frame.width, height: self.view.frame.height))
        
        mapViewController = UIStoryboard.mapViewController()
        mapViewController.delegate = self
        
        mapNavigationController = UINavigationController(rootViewController: mapViewController)
        view.addSubview(mapNavigationController.view)
        addChild(mapNavigationController)
        
        mapNavigationController.didMove(toParent: self)
        
        let panGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        panGestureRecognizer.edges = .right
        mapNavigationController.view.addGestureRecognizer(panGestureRecognizer)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ContainerViewController.closeMenuPanel(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        mapNavigationController.view.addGestureRecognizer(swipeRight)
    }

}

extension ContainerViewController: SideMenuDelegate {
    
    func toggleMenuPanel() {
        let notAlreadyExpanded = (currentState != .menuPanelExpanded)
        
        if notAlreadyExpanded {
            addMenuViewController()
        }
        
        animateMenuPanel(shouldExpand: notAlreadyExpanded)
    }
    
    func addMenuViewController() {
        guard menuViewController == nil else { return }
        
        if let vc = UIStoryboard.menuViewController() {
            addChildSidePanelController(vc)
            menuViewController = vc
            
            menuViewController?.view.frame = CGRect(x: CGFloat(self.view.frame.width), y: CGFloat(0), width: self.view.frame.width * menuWidth, height: self.view.frame.height)
            
            menuViewController?.delegate = self
            
        }
    }
    
    func addChildSidePanelController(_ sidePanelController: SideMenuViewController) {
        view.addSubview(sidePanelController.view)
        addChild(sidePanelController)
        sidePanelController.didMove(toParent: self)
    }
    
    @objc func closeMenuPanel(_ gesture: UISwipeGestureRecognizer) {
        animateMenuPanel(shouldExpand: false)
    }
    
    @objc func closeMenuPanelOnTapOutside(_ gesture: UITapGestureRecognizer) {
        animateMenuPanel(shouldExpand: false)
        debugPrint(gesture)
    }
    
    func animateMenuPanel(shouldExpand: Bool) {
        
        let swipeMenuRight = UISwipeGestureRecognizer(target: self, action: #selector(ContainerViewController.closeMenuPanel(_:)))
        let tapOutsideMenu = UITapGestureRecognizer(target: self, action: #selector(ContainerViewController.closeMenuPanelOnTapOutside(_:)))
        if shouldExpand {
            currentState = .menuPanelExpanded
            animateMenuPanelXPosition(
                targetPosition: self.view.bounds.width * (1-menuWidth))
            
            swipeMenuRight.direction = UISwipeGestureRecognizer.Direction.right
            menuViewController?.view.addGestureRecognizer(swipeMenuRight)
            overlay.addGestureRecognizer(tapOutsideMenu)
            self.view.insertSubview(overlay, belowSubview: menuViewController!.view)
        } else {
            menuViewController?.view.removeGestureRecognizer(swipeMenuRight)
            animateMenuPanelXPosition(targetPosition: self.view.bounds.width) { finished in
                self.currentState = .menuCollapsed
                self.menuViewController?.view.removeFromSuperview()
                self.menuViewController = nil
            }
            overlay.removeGestureRecognizer(tapOutsideMenu)
            self.overlay.removeFromSuperview()
        }
    }
    

    func animateMenuPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut, animations: {
                        self.menuViewController?.view.frame.origin.x = targetPosition
        }, completion: completion)
    }
    
}

extension ContainerViewController: UIGestureRecognizerDelegate {
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        let gestureIsDraggingFromRightToLeft = (recognizer.velocity(in: view).x < 0)
        
        switch recognizer.state {
            
        case .began:
            if currentState == .menuCollapsed {
                if gestureIsDraggingFromRightToLeft {
                    addMenuViewController()
                }
            }
            
        case .changed:
            if let menuView = self.menuViewController?.view {
                menuView.center.x = menuView.center.x + recognizer.translation(in: view).x
                recognizer.setTranslation(CGPoint.zero, in: view)
            }
            
        case .ended:
            if let menuView = menuViewController?.view {
                let hasMovedEnough = menuView.center.x < (view.bounds.size.width + menuView.frame.width / 2)
                animateMenuPanel(shouldExpand: hasMovedEnough)
            }
            
        default:
            break
        }
    }
}

private extension UIStoryboard {
    static func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    static func menuViewController() -> SideMenuViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
    }
    
    static func mapViewController() -> MapViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
    }
}
