import UIKit
import Foundation
import Alamofire
import SwiftKeychainWrapper
import Amplify
import AmplifyPlugins

class PasswordResetController: UIViewController {
    
    @IBOutlet var confirmationCode: UITextField!
    @IBOutlet var confirmedPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    
    var username: String = ""
    
    @IBAction func didTapResetPassword(_ sender: Any) {
        let newPasswordText = newPassword.text ?? ""
        let confirmedPasswordText = confirmedPassword.text ?? ""
        let dispatchGroup = DispatchGroup()
        
        if (newPasswordText.isEmpty || confirmedPasswordText.isEmpty) {
            return
        }
        
        if (newPasswordText != confirmedPasswordText) {
            displayAlert(message: "Paswords do not match")
            return
        }
        
        _ = Amplify.Auth.confirmResetPassword(
            for: username,
            with: newPasswordText,
            confirmationCode: confirmationCode.text ?? "") {(result) in
                switch result {
                case .success:
                    dispatchGroup.notify(queue: .main) {
                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let passwordChangedConfirmation = storyboard.instantiateViewController(withIdentifier: "PasswordChangedConfirmationController") as! PasswordChangedConfirmationController
                        UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                            UIApplication.shared.keyWindow?.rootViewController = passwordChangedConfirmation
                        })
                    }
                case .failure(let error):
                    switch error {
                    case AuthError.notAuthorized(let errorDescription, _, _):
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    case AuthError.invalidState(let errorDescription, _, _):
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    case AuthError.configuration(let errorDescription, _, _):
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    case AuthError.service(let errorDescription, _, let error):
                        switch error {
                        case AWSCognitoAuthError.invalidParameter?:
                            dispatchGroup.notify(queue: .main) {
                                self.displayAlert(message: "Passwords must be at least 8 characters and have at least one uppercase, lowercase, special character, and number")
                            }
                        case AWSCognitoAuthError.codeExpired?:
                            dispatchGroup.notify(queue: .main) {
                                self.displayAlert(message: "Your security code has expired")
                            }
                        case AWSCognitoAuthError.codeMismatch?:
                            dispatchGroup.notify(queue: .main) {
                                self.displayAlert(message: "Incorrect recovery code")
                            }
                        default:
                            dispatchGroup.notify(queue: .main) {
                                self.displayAlert(message: errorDescription)
                            }
                        }
                    case AuthError.sessionExpired(let errorDescription, _, _):
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    case AuthError.signedOut(let errorDescription, _, _):
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: errorDescription)
                        }
                    default:
                        dispatchGroup.notify(queue: .main) {
                            self.displayAlert(message: "We ran into a problem")
                        }
                    }
                }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func displayAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
