import UIKit


@objc
protocol SideMenuDelegate {
    @objc func toggleMenuPanel()
    @objc func animateMenuPanel(shouldExpand: Bool)
}
