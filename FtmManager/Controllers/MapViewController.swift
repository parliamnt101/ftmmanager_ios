//
//  ViewController.swift
//  FtmManager
//
//  Created by Julian Gallo on 2/25/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import Locksmith
import GoogleMaps
import AVFoundation
import QuartzCore
import Alamofire


extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet var MapBottom: NSLayoutConstraint!
    @IBOutlet var checkinButton: UIButton!    
    @IBOutlet var panBuffer: UIView!
    
    var firstMapUpdate = true

    var delegate: SideMenuDelegate?
    let locationManager = CLLocationManager()
    var checkinViewController: CheckinViewController?
    let checkinPanelHeight: CGFloat = 0.3
    
    let geocoder = GMSGeocoder()
    let descriptionDisplay = UITextView()
    var latitude: CLLocationDegrees? = nil
    var longitude: CLLocationDegrees? = nil
    var location:GMSAddress? = nil
    
    var profileImageSource: UIImage? = nil
    var truckNameText = ""
    var truckDescriptionText = ""
    var truckWebsiteText = ""
    var twitterHandle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.getProfileInfo()

        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.distanceFilter = 100.0
            locationManager.requestLocation()
        }
        
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: mapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        let mapPin = UIImageView()
        mapPin.image = UIImage(named: "mapPin")
        mapPin.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(mapPin)
        
        descriptionDisplay.isEditable = false
        descriptionDisplay.textAlignment = .center
        descriptionDisplay.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(descriptionDisplay)
        
        [
            mapPin.heightAnchor.constraint(equalToConstant: 20),
            mapPin.widthAnchor.constraint(equalToConstant: 20),
            mapPin.centerXAnchor.constraint(equalTo: mapView.centerXAnchor),
            mapPin.centerYAnchor.constraint(equalTo: mapView.centerYAnchor),
            
            descriptionDisplay.topAnchor.constraint(equalTo: self.checkinButton!.bottomAnchor),
            descriptionDisplay.heightAnchor.constraint(equalToConstant: self.view.frame.height),
            descriptionDisplay.widthAnchor.constraint(equalToConstant: self.view.frame.width),
        ].forEach{ $0.isActive = true}
        
        view.bringSubviewToFront(panBuffer)
        view.sendSubviewToBack(mapView)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MapViewController.postWasSuccessful),
                                               name: NSNotification.Name(rawValue: "postWasSuccessful"),
                                               object: nil
        )
    }
    
    func getProfileInfo() {
        cognitoSession.request(
            backend_server_url + "api/profile/"
        ){ URLRequest in URLRequest.timeoutInterval = 5}.validate(statusCode: 200..<300).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    self.truckNameText = json["truck_name"] as? String ?? ""
                    self.truckDescriptionText = json["truck_description"] as? String ?? ""
                    self.truckWebsiteText = json["truck_website"] as? String ?? ""
                    self.twitterHandle = json["twitter_account"] as? String ?? ""
                    let truck_image_url = json["truck_image"] as? String ?? ""
                    if truck_image_url != "" {
                        AF.request(truck_image_url).validate()
                            .responseData(completionHandler: { (responseData) in
                                self.profileImageSource = UIImage(data: responseData.data!)
                            })
                    }
                }
            case .failure(_):
                self.networkErrorMessage()
            }
        }
    }
    
    func networkErrorMessage() {
        let alert = UIAlertController(title: "Error", message: "Network Error", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        delegate?.animateMenuPanel(shouldExpand: false)
    }
    
    @IBAction func unwindToMap(_ unwindSegue: UIStoryboardSegue) {
        delegate?.animateMenuPanel(shouldExpand: false)
        self.getProfileInfo()
    }
    
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        let lastLocation = locations.last!
        updateMap(lastLocation: lastLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func updateMap(lastLocation: CLLocation) {
        if firstMapUpdate {
            firstMapUpdate = false
            mapView.camera = GMSCameraPosition.camera(withLatitude: locationManager.location!.coordinate.latitude,
                                                      longitude: locationManager.location!.coordinate.longitude,
                                                      zoom: 17.0)
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        geocoder.reverseGeocodeCoordinate(cameraPosition.target) { (response, error) in
            guard error == nil else {
                return
            }
            
            self.latitude = cameraPosition.target.latitude
            self.longitude = cameraPosition.target.longitude
            self.location = response?.results()![1]
            if let locationDescription = self.location?.thoroughfare {
                self.descriptionDisplay.text = locationDescription
            } else {
                self.descriptionDisplay.text = ""
            }
        }
    }
    
    @IBAction func MenuTapped(_ sender: Any) {
        delegate?.toggleMenuPanel()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checkIn" {
            if let checkinViewController = segue.destination as? CheckinViewController {
                checkinViewController.twitterLoggedIn = !twitterHandle.isEmpty
                checkinViewController.latitude = latitude
                checkinViewController.longitude = longitude
                checkinViewController.locationThoroughfare = location?.thoroughfare ?? ""
                checkinViewController.locationLocality =  location?.locality ?? ""
                checkinViewController.profileImageSource = self.profileImageSource
            }
        }
    }
    
    @objc func postWasSuccessful() {
        let alert = UIAlertController(title: "Checked in!", message: "Hungry customers are on the way!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
