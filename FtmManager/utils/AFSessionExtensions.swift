//
//  OAuthUtils.swift
//  FtmManager
//
//  Created by Julian Gallo on 6/1/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import Foundation
import OAuthSwift
import Alamofire
import SwiftKeychainWrapper
import Amplify
import AWSPluginsCore

let cognitoSession = Session(interceptor: RequestInterceptor())

final class RequestInterceptor: Alamofire.RequestInterceptor {
        
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        let authToken: String? = KeychainWrapper.standard.string(forKey: "authToken")
        var urlRequest = urlRequest
        
        urlRequest.setValue("Bearer " + (authToken ?? ""), forHTTPHeaderField: "Authorization")
        completion(.success(urlRequest))
    }
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 else {
            /// The request did not fail due to a 401 Unauthorized response.
            /// Return the original error and don't retry the request.
            return completion(.doNotRetryWithError(error))
        }
        
        _ = Amplify.Auth.fetchAuthSession { (result) in
            do {
                let session = try result.get()
                // Get cognito user pool token
                if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                    let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                    KeychainWrapper.standard.set(tokens.idToken, forKey: "authToken")
                    completion(.retry)
                }
            } catch {
                print("Fetch auth session failed with error - \(error)")
                completion(.doNotRetryWithError(error))
            }
        }
    }
}
