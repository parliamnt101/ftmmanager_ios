//
//  TweetViewController.swift
//  FtmManager
//
//  Created by Julian Gallo on 6/9/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation
import Alamofire
import SwiftKeychainWrapper


class CheckinViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    var latitude: CLLocationDegrees? = nil
    var longitude: CLLocationDegrees? = nil
    var locationThoroughfare = ""
    var locationLocality = ""
    var profileImageSource: UIImage? = nil
    var twitterLoggedIn: Bool = false
    
    let imagePicker = UIImagePickerController()
    var cameraAlert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    
    let datePicker = UIDatePicker()

    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var toolbar: UIToolbar!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var twitterSwitch: UISwitch!
    @IBOutlet var checkInMessage: UITextView!
    @IBOutlet var shareOnTwitterText: UITextField!
    @IBOutlet var shareOnTwitterButton: UISwitch!
    @IBOutlet var checkinExpiry: UITextField!
    @IBOutlet var postImageWidth: NSLayoutConstraint!
    @IBOutlet var postImageHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        createDatePicker()
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        
        let calendar = Calendar.current
        let rightNow = Date()
        let interval = 30
        let nextDiff = interval - calendar.component(.minute, from: rightNow) % interval
        let nextDate = calendar.date(byAdding: .minute, value: nextDiff, to: rightNow) ?? Date()
        let defaultExpiry = nextDate.addingTimeInterval(4*60*60)
        
        datePicker.date = defaultExpiry
        checkinExpiry.text = formatter.string(from: defaultExpiry)
        
        checkInMessage.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        checkInMessage.delegate = self
        
        view.backgroundColor = .white
        
        if locationThoroughfare != "" && locationLocality != "" {
            checkInMessage.text = "We are currently parked at " + locationThoroughfare + " in " + locationLocality
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }
        cameraAlert.addAction(cameraAction)
        cameraAlert.addAction(galleryAction)
        cameraAlert.addAction(cancelAction)
        cameraAlert.popoverPresentationController?.sourceView = self.view
        
        if (profileImageSource != nil) {
            profileImage.image = profileImageSource
            profileImage.layer.cornerRadius = profileImage.frame.width/2.0
            profileImage.clipsToBounds = true
        }
        
        toolbar.removeFromSuperview()

        if (!twitterLoggedIn) {
            shareOnTwitterText.removeFromSuperview()
            shareOnTwitterButton.removeFromSuperview()
        } else {
            checkInMessage.inputAccessoryView = toolbar
        }
        
        checkInMessage.isScrollEnabled = true
    }
    
    func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneBtn], animated: true)
        checkinExpiry.inputAccessoryView = toolbar
        datePicker.datePickerMode = .time
        datePicker.minuteInterval = 30
        datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        
        checkinExpiry.textAlignment = .center
        checkinExpiry.inputView = datePicker
    }
    
    @objc func datePickerChanged(picker: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        checkinExpiry.text = formatter.string(from: datePicker.date)
    }
    
    @objc func donePressed() {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        checkinExpiry.text = formatter.string(from: datePicker.date)
        checkinExpiry.endEditing(true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 280
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.checkInMessage.becomeFirstResponder()
        }
    }
    
    @IBAction func postWasPressed(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
             switch CLLocationManager.authorizationStatus() {
                case .authorizedAlways, .authorizedWhenInUse:
                    var imageToUpload: Data? = nil
                    let textToUpload = self.checkInMessage.text!.data(using: String.Encoding.utf8)!
                    
                    var end = "".data(using: String.Encoding.utf8)!
                                        
                    if (!checkinExpiry.text!.isEmpty) {
                        let endRaw = self.datePicker.date
                        let formatter = ISO8601DateFormatter()
                        formatter.timeZone = TimeZone.init(identifier: "America/New_York")
                        formatter.formatOptions = [.withFullDate, .withTime, .withSpaceBetweenDateAndTime, .withColonSeparatorInTime]
                        end = formatter.string(from: endRaw).data(using: String.Encoding.utf8)!
                    }
                    
                    if self.postImage.image != nil {
                        let sizeInBytes = 5 * 1024 * 1024
                        var needCompress:Bool = true
                        var compressingValue:CGFloat = 1.0
                        while (needCompress && compressingValue > 0.0) {
                            if let data:Data = self.postImage.image!.jpegData(compressionQuality: compressingValue) {
                                if data.count < sizeInBytes {
                                    needCompress = false
                                    imageToUpload = data
                                } else {
                                    compressingValue -= 0.1
                                }
                            }
                        }
                    }
                    AF.session.configuration.timeoutIntervalForResource = 10
                    cognitoSession.upload(
                        multipartFormData: {
                            multipartFormData in
                            multipartFormData.append(textToUpload, withName: "status", mimeType: "text")
                            multipartFormData.append(end, withName: "end", mimeType: "text")
                            multipartFormData.append(String(self.latitude!).data(using: String.Encoding.utf8)!, withName: "latitude", mimeType: "text")
                            multipartFormData.append(String(self.longitude!).data(using: String.Encoding.utf8)!, withName: "longitude", mimeType: "text")
                            multipartFormData.append(String(self.twitterSwitch.isOn).data(using: String.Encoding.utf8)!, withName: "post_to_twitter", mimeType: "text")
                            multipartFormData.append(String(self.locationThoroughfare).data(using: String.Encoding.utf8)!, withName: "address")
                            if imageToUpload != nil {
                                multipartFormData.append(imageToUpload!, withName: "image", fileName: "image.png", mimeType: "image/png")
                            }
                            else {}
                    },
                        to: "\(backend_server_url)api/check-in/"
                    ).validate(statusCode: 200..<300).response { response in
                        switch response.result {
                        case .success(_):
                            self.checkInMessage.endEditing(true)
                            self.dismiss(animated: true, completion: {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postWasSuccessful"), object: nil)
                            })
                        case .failure(_):
                            self.networkErrorMessage()
                        }
                }
                case .notDetermined, .restricted, .denied:
                    noLocationErrorMessage()
            }
        } else {
            noLocationErrorMessage()
        }

    }
    
    func networkErrorMessage() {
        let alert = UIAlertController(title: "Error", message: "Network Error", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func noLocationErrorMessage() {
        let message = "Location not found. Allow location access in your device settings."
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backWasPressed(_ sender: Any) {
        checkInMessage.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        cameraAlert.dismiss(animated: true, completion: nil)
        let cameraAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
        if cameraAvailable {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = true
                present(imagePicker, animated: true, completion: nil)
                
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = true
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                
            case .denied, .restricted:
                let alert = UIAlertController(title: "Wartning", message: "This app doesn't have permission to access your camera. Please grant access in your device's settings to proceed.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Wartning", message: "No camera found", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        cameraAlert.dismiss(animated: true, completion: nil)
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cameraWasPressed(_ sender: UIBarButtonItem) {
        // Add the actions
        imagePicker.delegate = self
        present(cameraAlert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        let image :UIImage
        if info[.editedImage] != nil {
            image = info[.editedImage] as! UIImage
        } else {
            image = info[.originalImage] as! UIImage
        }

        postImage.image = image
        postImage.layer.cornerRadius = 8
        postImage.clipsToBounds = true
        postImage.translatesAutoresizingMaskIntoConstraints = false
        postImage.isUserInteractionEnabled = true
        
        let xButton = UIButton(type: .custom)
        xButton.setImage(UIImage(named: "xButton"), for: .normal)
        xButton.translatesAutoresizingMaskIntoConstraints = false
        xButton.addTarget(self, action: #selector(self.closeImageWasPressed), for: UIControl.Event.touchUpInside)
        postImage.addSubview(xButton)
        
        let aspectRatio = image.size.width / image.size.height
        let new_width = aspectRatio * 100
        
        postImageWidth.constant = new_width
        postImageHeight.constant = 100
        postImage.layoutIfNeeded()
        
        [
            xButton.topAnchor.constraint(equalTo: postImage.topAnchor, constant: 5),
            xButton.trailingAnchor.constraint(equalTo: postImage.trailingAnchor, constant: -5),
            xButton.heightAnchor.constraint(equalToConstant: 20),
            xButton.widthAnchor.constraint(equalToConstant: 20),
        ].forEach{ $0.isActive = true}

    }
    
    @objc func closeImageWasPressed() {
        postImage.image = nil
        postImage.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat

        return dateFormatter.string(from: dt!)
    }
}
