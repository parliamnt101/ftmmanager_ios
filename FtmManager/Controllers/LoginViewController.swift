//
//  LoginViewController.swift
//  FtmManager
//
//  Created by Julian Gallo on 5/15/19.
//  Copyright © 2019 Julian Gallo. All rights reserved.
//

import UIKit
import QuartzCore
import SwiftKeychainWrapper
import Alamofire
import Amplify
import AWSPluginsCore

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    var containerTextField: UITextField!
    
    var passwordReset: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(red: 232/255, green: 23/255, blue: 120/255, alpha: 1.0)
        emailTextField.layer.borderColor = borderColor.cgColor
        passwordTextField.layer.borderColor = borderColor.cgColor
        emailTextField.layer.borderWidth = 1.0
        passwordTextField.layer.borderWidth = 1.0
        
        self.hideKeyboardWhenTappedAround()
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.emailTextField.tag = 0
        self.passwordTextField.tag = 1
        
        containerTextField = UITextField()
        containerTextField.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 30)
        
        emailTextField.addTarget(self, action: #selector(emailFocus(_:)), for: .editingDidBegin)
        emailTextField.addTarget(self, action: #selector(updateInputContainerView(_:)), for: .editingChanged)

        passwordTextField.addTarget(self, action: #selector(passwordFocus(_:)), for: .editingDidBegin)
        passwordTextField.addTarget(self, action: #selector(updateInputContainerView(_:)), for: .editingChanged)
    }
    
    
    @objc func updateInputContainerView(_ textField: UITextField) {
        containerTextField.text = textField.text
    }
   
    @objc func emailFocus(_ textField: UITextField) {
        containerTextField.text = textField.text
        containerTextField.isSecureTextEntry = false
    }
    
    @objc func passwordFocus(_ textField: UITextField) {
        containerTextField.text = textField.text
        containerTextField.isSecureTextEntry = true
    }
    
    lazy var inputContainerView: UIView = {
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 30)
        containerView.backgroundColor = UIColor.lightGray

        containerView.addSubview(containerTextField)
        containerTextField.translatesAutoresizingMaskIntoConstraints = false
        containerTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 5).isActive = true

        return containerView
    }()
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        self.attemptLogin()
    }
    
    @IBAction func unwindToLogin(_ unwindSegue: UIStoryboardSegue) {
    }
    
    func attemptLogin() {
        let username = emailTextField.text!
        let password = passwordTextField.text!
        view.endEditing(true)
        
        let dispatchGroup = DispatchGroup()
        
        _ = Amplify.Auth.signIn(username: username, password: password) { result in
            switch result {
            case .success(let success):
                if case .confirmSignInWithNewPassword(_) = success.nextStep {
                    dispatchGroup.notify(queue: .main) {
                        let storyBoard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let initialPasswordSetController = storyBoard.instantiateViewController(withIdentifier: "InitialPasswordSetController") as! InitialPasswordSetController
    
                        UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                            UIApplication.shared.keyWindow?.rootViewController = initialPasswordSetController
                        })
                    }
                } else {
                    _ = Amplify.Auth.fetchAuthSession { (result) in
                        do {
                            let session = try result.get()
                            // Get cognito user pool token
                            if let cognitoTokenProvider = session as? AuthCognitoTokensProvider {
                                let tokens = try cognitoTokenProvider.getCognitoTokens().get()
                                KeychainWrapper.standard.set(tokens.idToken, forKey: "authToken")
                                dispatchGroup.notify(queue: .main) {
                                    let storyBoard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let containerViewController = storyBoard.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
                                    
                                    UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                                        UIApplication.shared.keyWindow?.rootViewController = containerViewController
                                    })
                                }
                            }
                        } catch {
                            print("Fetch auth session failed with error - \(error)")
                        }
                    }
                }
            case .failure(_):
                let dispatchGroup = DispatchGroup()
                dispatchGroup.notify(queue: .main) {
                    let animation = CABasicAnimation(keyPath: "position")
                    animation.duration = 0.07
                    animation.repeatCount = 2
                    animation.autoreverses = true
                    animation.fromValue = NSValue(cgPoint: CGPoint(x: self.passwordTextField.center.x - 10, y: self.passwordTextField.center.y))
                    animation.toValue = NSValue(cgPoint: CGPoint(x: self.passwordTextField.center.x + 10, y: self.passwordTextField.center.y))

                    
                    self.passwordTextField.layer.add(animation, forKey: "position")
                    self.passwordTextField.text = ""
                }
            }
        }
    }
    
    func networkErrorMessage() {
        let alert = UIAlertController(title: "Error", message: "Network Error", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
